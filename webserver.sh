#!/bin/bash

if (( $#  > 0 ))
then
    hostname=$1
else
    echo "WTF: you must supply a hostname or IP address" 1>&2 
    exit 1 
fi 

ssh -o StrictHostKeyChecking=no -i ~/.ssh/TanyaMollerKey.pem ec2-user@$hostname '
sudo yum -y install httpd
if rpm -qa | grep '^httpd-[0-9]' >/dev/null 2>&1
then
  sudo systemctl start httpd
else
    exit 1
fi

sudo sh -c "cat >/var/www/html/index.html <<_END_
<h1> Hey server 1 </h1>
<img src=\"https://i.pinimg.com/originals/5d/50/96/5d50964c25f8ce13e5531c60892c03dd.gif\">
_END_"
'